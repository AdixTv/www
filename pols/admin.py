from django.contrib import admin
from .models import *


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'is_open_question', 'pub_date')
    search_fields = ('question_text',)
    date_hierarchy = 'pub_date'


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'choice_text', 'votes')
    search_fields = ('question__question_text', 'choice_text')

    def question_text(self, obj):
        return obj.question.question_text


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'answer')
    search_fields = ('answer', 'question__question_text')

    def question_text(self, obj):
        return obj.question.question_text
